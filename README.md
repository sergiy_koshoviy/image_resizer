## Image Resizer

### API

#### POST /api/images/resize
##### Required params:
  - images: {name: 'image_name', url: 'image_url'}
  
##### Responce: 
  - body:   'Image has no problems and can be processed'
  - status: 200

##### Result:
  - resize image and save to new file
  - set price by image resize
  
##### Errors:
  - body:   'Some message of errors'
  - status: 422
  
#### GET /api/images
##### Required params:
  - no

##### Responce: 
  - body:   Images list
    for example: 
    [
        {"name":"asdf","url":"http://mixandgo.com/external_apis_in_rails.jpg","price":0.001},
        {"name":"asdf","url":"http://mixandgo.com/uploads/external_apis_in_rails.jpg","price":0.001}
    ]    
  - status: 200

##### Result:
  - return list of resized images
