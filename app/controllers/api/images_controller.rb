class Api::ImagesController < ApplicationController
  skip_before_action :verify_authenticity_token

  # GET /api/images
  # return list of resized images
  def index
    @images = Image.all.map { |image| { name:  image.name,
                                        url:   image.url,
                                        price: image.price
                                      }
    }

    render json: @images
  end

  # POST /api/images/resize
  # params images: {name: 'image_name', url: 'image_url'}
  # resize image and save to new file
  # set price by image resize
  def resize
    is_params_correct = check_params(params[:image])

    unless is_params_correct[:status]
      render json: is_params_correct[:errors], status: 400 and return
    end

    result = resize_image

    if result[:status]
      render json: 'Image has no problems and can be processed', status: 200
    else
      render json: result[:error], status: :unprocessable_entity
    end
  end

  private
  # handle bad params
  def check_params(attr)
    errors = ''

    errors << 'Image name can\'t be blank; ' if attr.blank? || attr[:name].blank?
    errors << 'Image url can\'t be blank;' if attr.blank? || attr[:url].blank?
    errors << 'Image url is not valid;' if attr.present? && attr[:url].present? && !is_url_valid(attr[:url])

    { status: errors.blank?, errors: errors }
  end

  def is_url_valid(url)
    response = Net::HTTP.get_response(URI.parse(url))

    case response.content_type
      when 'image/png', 'image/gif', 'image/jpeg'
        true
      else
        false
    end
  rescue
    false
  end

  def resize_image
    ImageResizeService.new({
                               name:  params[:image][:name],
                               url:   params[:image][:url],
                               width: image_type_icon.width_in_pixel
                           }).resize
  end

  def image_type_icon
    ImageType.find_by_name('icon')
  end
end
