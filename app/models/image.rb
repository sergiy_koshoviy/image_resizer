class Image < ApplicationRecord
  validates :name, presence: true, uniqueness: true, length: { maximum: 60 }
  validates :url, presence: true
  validates :original_url, presence: true
  validates :original_size, presence: true, numericality: { :greater_than => 0, less_than: 2147483647 }
  validates :image_format, presence: true, length: { maximum: 60 }

  def price
    price = Image.default_price

    price_by_size   = get_price_by_size
    price_by_format = get_price_by_format

    price           = price_by_size if price_by_size && price_by_size > price
    price           = price_by_format if price_by_format && price_by_format > price

    price
  end


  def get_price_by_size
    prices_by_size = ResizePrice.prices_by_size.order('DESC image_size')
    prices_by_size.find_each do |resize_price|
      return resize_price.price if original_size && original_size > resize_price.image_size
    end
  end

  def get_price_by_format
    image_format ? ResizePrice.find_by_image_type(image_format).try(:price) : nil
  end

  def self.default_price
    ResizePrice.default_price.try(:price)
  end
end
