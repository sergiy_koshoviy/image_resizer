class ResizePrice < ApplicationRecord
  scope :prices_by_size, -> { where('image_size is not null') }
  scope :prices_by_format, -> { where('image_type is not null') }

  def self.default_price
    find_by_name('default_price')
  end
end
