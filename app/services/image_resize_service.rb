require 'rmagick'

class ImageResizeService
  attr_reader :name, :url, :width

  def initialize(params)
    @name  = params[:name]
    @url   = params[:url]
    @width = params[:width]
  end

  def resize
    # all attributes original image
    get_image = get_image_attr

    return { status: false, error: get_image[:error], image: nil } unless get_image[:status]

    original_image = get_image[:file]
    # set attributes resized image
    new_width      = width
    new_height     = original_image[:height]/(original_image[:width]/width.to_f)

    # create resized image
    new_image      = original_image[:image].resize(new_width, new_height)
    new_image_name = "#{name.parameterize}-#{time_creating}.#{original_image[:extention]}"
    new_image_url  = "resized_images/#{new_image_name}"

    begin
      # save to db information about resized image
      resized_image = image_to_save.new({
                                            name:          new_image_name,
                                            url:           new_image_url,
                                            original_url:  url,
                                            original_size: original_image[:size],
                                            image_format:  original_image[:image_format]
                                        })
      if resized_image.save
        new_image.write("public/#{new_image_url}")

        { status: true, error: nil, image: resized_image }
      else
        { status: false, error: resized_image.errors.full_messages }
      end
    rescue StandardError => e
      Rails.logger.info e.message
      { status: false, error: e.message }
    end
  end

  private

  def image_to_save
    Image
  end

  def time_creating
    Time.zone.now.to_formatted_s(:number)
  end

  def get_image_attr
    begin
      image = Magick::Image.read(url).first

      filename = image.filename.split('?').first
      file     = {
          image:        image,
          extention:    filename.split('.').last,
          size:         image.filesize,
          image_format: image.format,
          width:        image.columns,
          height:       image.rows
      }

      { status: true, error: nil, file: file }
    rescue StandardError => e
      Rails.logger.info e.message
      { status: false, error: e.message, file: nil }
    end
  end
end