class CreateImageTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :image_types do |t|
      t.string :name
      t.integer :width_in_pixel

      t.timestamps
    end
  end
end
