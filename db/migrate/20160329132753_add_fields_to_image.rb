class AddFieldsToImage < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :original_url, :string
    add_column :images, :original_size, :integer
    add_column :images, :format, :string
  end
end
