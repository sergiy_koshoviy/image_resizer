class CreateResizePrices < ActiveRecord::Migration[5.0]
  def change
    create_table :resize_prices do |t|
      t.string :name
      t.integer :image_size
      t.string :image_type
      t.integer :price

      t.timestamps
    end
  end
end
