class RenameFormatToImageFormatForImage < ActiveRecord::Migration[5.0]
  def change
    rename_column :images, :format, :image_format
  end
end
