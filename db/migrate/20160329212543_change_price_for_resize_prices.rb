class ChangePriceForResizePrices < ActiveRecord::Migration[5.0]
  def change
    change_column :resize_prices, :price, :float
  end
end
