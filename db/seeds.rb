# Create ImageType
image_types = [
    {name: 'icon', width_in_pixel: 15},
]

image_types.each {|type| ImageType.find_or_create_by(type)}

# Create ResizePrice
resize_price = [
    {name: 'default_price', image_size: nil, image_type: nil, price: 0.001},
    {name: '100kb', image_size: 100000, image_type: nil, price: 0.01},
    {name: 'gif', image_size: nil, image_type: 'GIF', price: 0.005},
]

resize_price.each {|price| ResizePrice.find_or_create_by(price)}