require 'rails_helper'

RSpec.describe 'Image', type: :request do
  context 'Resize image' do
    it 'return successful resize' do
      params = { name: 'new_image_name', url: ENV['DEFAULT_IMG'] }
      post '/api/images/resize', params: { image: params }

      expect(response.body).to eq('Image has no problems and can be processed')
      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(200)
    end

    it 'should be error about invalid image data' do
      params = { name: 'new_image_name'*100, url: ENV['DEFAULT_IMG'] }
      post '/api/images/resize', params: { image: params }

      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(422)
      expect(response.body).to_not eq('Image has no problems and can be processed')
    end

    it 'error params \'name\' and \'url\' are required' do
      post '/api/images/resize'

      expect(response).to have_http_status(400)
      expect(response.body).to eq('Image name can\'t be blank; Image url can\'t be blank;')
    end

    it 'error: params \'name\' is required' do
      params = { url: ENV['DEFAULT_IMG'] }
      post '/api/images/resize', params: { image: params }

      expect(response).to have_http_status(400)
      expect(response.body).to eq('Image name can\'t be blank; ')
    end

    it 'error: params \'url\' is required' do
      params = { name: 'new_image_name' }
      post '/api/images/resize', params: { image: params }

      expect(response).to have_http_status(400)
      expect(response.body).to eq('Image url can\'t be blank;')
    end

    it 'error: params \'url\' is invalid' do
      params = { name: 'new_image_name', url: 'testsomesite.com/test_image.png' }
      post '/api/images/resize', params: { image: params }

      expect(response).to have_http_status(400)
      expect(response.body).to eq('Image url is not valid;')
    end

    it 'error: params \'url\' is valid but not image' do
      params = { name: 'new_image_name', url: 'http://google.com' }
      post '/api/images/resize', params: { image: params }

      expect(response).to have_http_status(400)
      expect(response.body).to eq('Image url is not valid;')
    end
  end

  context 'list' do
    it 'should return images list' do
      image = FactoryGirl.create :image
      get '/api/images'

      expect(response).to have_http_status(:success)
      expect(response.body).to eq([{
                                       name:  image.name,
                                       url:   image.url,
                                       price: image.price
                                   }].to_json)
    end
  end

  context 'billing' do
    it 'should return image with price 0.001 usd' do
      params = { name: 'new_image_name', url: ENV['DEFAULT_IMG'] }
      post '/api/images/resize', params: { image: params }
      get '/api/images'

      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).first['price']).to eq(0.001)
    end

    it 'should return image with price 0.01 usd' do
      params = { name: 'new_image_name', url: ENV['BIG_IMG'] }
      post '/api/images/resize', params: { image: params }
      get '/api/images'

      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).first['price']).to eq(0.01)
    end

    it 'should return image with price 0.005 usd' do
      params = { name: 'new_image_name', url: ENV['SMALL_GIF_IMG'] }
      post '/api/images/resize', params: { image: params }
      get '/api/images'

      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).first['price']).to eq(0.005)
    end
  end
end