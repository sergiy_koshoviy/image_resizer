FactoryGirl.define do
  factory :image do
    name { Faker::Lorem.word }
    url 'resized_images/test_image.png'
    original_url ENV['DEFAULT_IMG']
    original_size { Faker::Number.number(4) }
    image_format 'PNG'
  end

  factory :default_image, class: Image do
    name { Faker::Lorem.word }
    url 'resized_images/test_image.png'
    original_url ENV['DEFAULT_IMG']
    original_size { Faker::Number.number(4) }
    image_format 'PNG'
  end

  factory :gif_image, class: Image do
    name { Faker::Lorem.word }
    url 'resized_images/test_image.gif'
    original_url ENV['SMALL_GIF_IMG']
    original_size { Faker::Number.number(4) }
    image_format 'GIF'
  end

  factory :big_image, class: Image do
    name { Faker::Lorem.word }
    url 'resized_images/test_image.gif'
    original_url ENV['BIG_IMG']
    original_size { Faker::Number.number(7) }
    image_format 'GIF'
  end
end
