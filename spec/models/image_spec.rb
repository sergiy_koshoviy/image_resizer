require 'rails_helper'

RSpec.describe Image, type: :model do
  context '#price' do
    it 'should return price of resize small image 0.001 usd' do
      image = FactoryGirl.create :default_image
      expect(image.price).to eq(0.001)
    end

    it 'should return price of resize small gif image 0.005 usd' do
      image = FactoryGirl.create :gif_image
      expect(image.price).to eq(0.005)
    end

    it 'should return price of resize big image 0.01 usd' do
      image = FactoryGirl.create :big_image
      expect(image.price).to eq(0.01)
    end
  end

  context 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :url }
    it { is_expected.to validate_presence_of :original_url }
    it { is_expected.to validate_presence_of :original_size }
    it { is_expected.to validate_presence_of :image_format }

    it { is_expected.to validate_length_of :name }
    it { is_expected.to validate_length_of :image_format }
    it { is_expected.to validate_length_of :original_size }

    it { is_expected.to validate_numericality_of :original_size }

    it { is_expected.to validate_uniqueness_of :name }
  end
end
