# https://relishapp.com/rspec/rspec-rails/docs
ENV['RAILS_ENV']     ||= 'test'
ENV['DEFAULT_IMG']   = 'https://bytebucket.org/sergiy_koshoviy/image_resizer/raw/8450383d9f012ec31e04e2b971a24f998eadbea3/public/test_image.png?token=4699739d4dc196e5a40550882ad421f9416985b3'
ENV['BIG_IMG']       = 'https://bytebucket.org/sergiy_koshoviy/image_resizer/raw/8450383d9f012ec31e04e2b971a24f998eadbea3/public/test_image.gif?token=46d6f8b91851478da4e89237b6b7606ed476aaeb'
ENV['SMALL_GIF_IMG'] = 'https://bytebucket.org/sergiy_koshoviy/image_resizer/raw/8450383d9f012ec31e04e2b971a24f998eadbea3/public/test_small_image.gif?token=8f833592fb78a558a361a692b7d295c1eba7c453'

require 'spec_helper'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'shoulda/matchers'

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.fixture_path               = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!

  config.before(:all) do
    Rails.application.load_seed # loading seeds
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end