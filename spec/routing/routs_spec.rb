require 'rails_helper'

RSpec.describe 'routing to images', type: :routing do
  it 'routes /api/images/resize to api/images#resize with params' do
    expect(post: '/api/images/resize').to route_to(
                                              controller: 'api/images',
                                              action:     'resize',
                                              default:    { 'format' => 'json' }
                                          )
  end

  it 'routes /api/images to api/images#index' do
    expect(get: '/api/images').to route_to(
                                      controller: 'api/images',
                                      action:     'index',
                                      default:    { 'format' => 'json' }
                                  )
  end
end