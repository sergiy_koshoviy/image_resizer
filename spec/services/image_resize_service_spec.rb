require 'rails_helper'

RSpec.describe ImageResizeService, type: :service do
  let(:image_resize_service) { ImageResizeService.new({
                                                          name:  Faker::Lorem.word,
                                                          url:   ENV['DEFAULT_IMG'],
                                                          width: ImageType.find_by_name('icon').width_in_pixel
                                                      }) }

  context '#resize' do
    it 'should be resized width to 15px' do
      resized_image = image_resize_service.resize[:image]

      image = Magick::Image.read("http://localhost:3000/#{resized_image.url}").first

      expect(image.columns).to eq(image_resize_service.width)
    end

    it 'should be resized and save proportion' do
      resized_image = image_resize_service.resize[:image]

      image          = Magick::Image.read("http://localhost:3000/#{resized_image.url}").first
      original_image = Magick::Image.read(image_resize_service.url).first

      expect(image.columns/image.rows).to eq(original_image.columns/original_image.rows)
    end

    it 'should return name too long' do
      resized_image = ImageResizeService.new({
                                                 name:  'image_name'*10,
                                                 url:   ENV['DEFAULT_IMG'],
                                                 width: ImageType.find_by_name('icon').width_in_pixel
                                             }).resize

      expect(resized_image[:status]).to eq(false)
      expect(resized_image[:error]).to eq(['Name is too long (maximum is 60 characters)'])
    end

    it 'should return error cann\'t find image' do
      resized_image = ImageResizeService.new({
                                                 name:  Faker::Lorem.word,
                                                 url:   'http://asdf.asdf/asdf.jpg',
                                                 width: ImageType.find_by_name('icon').width_in_pixel
                                             }).resize

      expect(resized_image[:status]).to eq(false)
      expect(resized_image[:error]).to include('no data returned')
    end
  end
end
